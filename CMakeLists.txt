cmake_minimum_required(VERSION 3.26)

project(test VERSION 0.1 LANGUAGES CXX)

include(FetchContent)

FetchContent_Declare(
  fastdds
  URL https://github.com/eProsima/Fast-DDS/archive/refs/tags/v2.12.1.tar.gz
)

FetchContent_MakeAvailable(fastdds)
